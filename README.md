![Hoppr Examples](https://gitlab.com/hoppr/hoppr/-/raw/dev/media/hoppr-repo-banner.png)

# Hoppr Continuous Delivery Pipeline

This is a demo of a [continuous-delivery-pipeline](https://gitlab.com/hoppr/examples/continuous-delivery-demo/continuous-delivery-pipeline)
to illustrate how [Hoppr](https://hoppr.dev/) can be used to create a multiple team
delivery pipeline into a secure environment.

## Concepts

The demo showcases several concepts when combined create a configuration managed and reproducible transfer into a
secure environment.

- Multiple teams defining dependencies using CycloneDX SBOMS and Hoppr Manifests in independent gitlab projects 
  - [sample-container-dependencies](https://gitlab.com/hoppr/examples/continuous-delivery-demo/sample-container-dependencies) - A development team
  declaring container dependencies used in a secure environment
  - [sample-dev-tools](https://gitlab.com/hoppr/examples/continuous-delivery-demo/sample-dev-tools) - A tools management team declaring
  what applications are needed in a secure environment
- The transfer process is controlled by a parent 

<div class="center">
```mermaid
graph TD
    sample-container-dependencies -->|container images| continuous-delivery-pipeline
    sample-dev-tools -->|dev tool binaries| continuous-delivery-pipeline
```
</div>

This example project shows several key concepts:

- [Renovate](https://github.com/renovatebot/renovate) updates a CycloneDX [sbom.json](sbom.json) when helm releases occur
  - This update triggers merge request to the `main` branch
- The merge to `main` triggers CI including [Semantic Release](https://github.com/semantic-release/semantic-release)
  - CycloneDX SBOM is created
  - __Semantic Release__ updates SBOM semantic versions in [manifest.yml](manifest.yml)
  - __Semantic Release__ commits changes back `main` branch
  - __Semantic Release__ tags repository
- __Renovate__ will detect __semantic versioned releases__ on this project and update the 
[continuous-delivery-pipeline](https://gitlab.com/hoppr/examples/continuous-delivery-demo/continuous-delivery-pipeline)
demo

## Key Files

- Automation
  - [renovate.json](renovate.json) - [Renovate](https://github.com/renovatebot/renovate) configuration for this
  project, and configures Renovate MRs to "automerge" and tracks helm releases
  - [.releaserc.yml](.releaserc.ym) - [Semantic Release](https://github.com/semantic-release/semantic-release) 
  configuration for this project, and configures where to "update" the SBOM release location in the [manifest.yml] file
- Hoppr
  - [containers.cdx.json](containers.cdx.json) - CycloneDX SBOM updated by Renovate
  - [manifest.yml](manifest.yml) - Hoppr manifest file detailing what SBOMs to use and what repositories to pull from
  - (optional) [transfer.yml](transfer.yml) - Only used if you want to run Hoppr on this sub-project only

## Standalone Commands

> Try it out with [gitpod](https://gitpod.io/#https://gitlab.com/hoppr/examples/continuous-delivery-demo/sample-dev-tools/-/tree/main/)!

If you would like to run this sub-project alone, the command is ...

```bash
hopctl bundle manifest.yml --transfer transfer.yml
```
